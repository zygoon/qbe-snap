<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Zygmunt Krynicki <me@zygoon.pl>
-->

# Snapcraft packaging for QBE

This repository contains snapcraft packaging for QBE.

I've tried submitting it upstream but my contribution was robbed of the chance
of discussion, due to the apparent prejudice of the maintainer.

Since the original license grants me sufficient rights, and others may find
this package useful, I am publishing the snap package on my own accord.
